package com.example.quintero.practica_06;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    Button save,load;
    EditText message, correo, tel;
    String Message;
    int data_block = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        save = (Button)findViewById(R.id.SAVE);
        load = (Button)findViewById(R.id.LOAD);

        message = (EditText)findViewById(R.id.nombre);
        correo = (EditText)findViewById(R.id.correo);
        tel = (EditText)findViewById(R.id.numero);



        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Message = "Nombre: "+message.getText().toString();
                Message += ", Correo: ";
                Message += correo.getText().toString();
                Message += ", Tel: ";
                Message += tel.getText().toString();
                try {
                    FileOutputStream fou = openFileOutput("text.txt", MODE_WORLD_READABLE);
                    OutputStreamWriter osw = new OutputStreamWriter(fou);
                    try {
                        osw.write(Message);
                        osw.flush();
                        osw.close();
                        Toast.makeText(getBaseContext(), "Dato guardado", Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });





        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    FileInputStream fis = openFileInput("text.txt");
                    InputStreamReader isr = new InputStreamReader(fis);
                    char[] data = new char[data_block];
                    String final_data ="";
                    int size;
                    try {
                        while((size = isr.read(data))>0){
                            String read_data = String.valueOf(data,0,size);
                            final_data += read_data;
                            data = new char[data_block];
                        }
                        Toast.makeText(getBaseContext(),"Dato: "+final_data,Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });













    }








}
